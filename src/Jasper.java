public class Jasper {
    private final Factory tehdas = new BossFactory();

    public String toString(){
        return String.format("Olen Jasper Java-koodaaja, ja minulla on päälläni %s, %s, %s ja %s.", tehdas.createFarmarit(), tehdas.createTpaita(), tehdas.createLippis(), tehdas.createKengät());
    }
}
