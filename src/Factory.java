import clothes.*;

public interface Factory {

    Lippis createLippis();

    Kengät createKengät();

    Tpaita createTpaita();

    Farmarit createFarmarit();
}
